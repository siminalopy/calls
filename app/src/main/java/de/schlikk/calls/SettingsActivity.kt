package de.schlikk.calls

import android.os.Bundle



class SettingsActivity : PermissibleActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings, SettingsFragment(this))
                .commit()
    
        requestEssentialPermission()
    }
    
    fun requestEssentialPermission() {
        val uniFormatter = UniFormatter( this )
        val call = PermissionCall( "android.permission.READ_PHONE_STATE", this, R.string.permissionReadPhoneState )
        call.positiveActionButton = uniFormatter.format( R.string.askAgain ) to ::requestEssentialPermission
        call.negativeActionButton = uniFormatter.format( R.string.cancelPermission ) to ::closeActivityBecauseOfPermissionDenial
        
        callUnderPermission( call )
    }
}
