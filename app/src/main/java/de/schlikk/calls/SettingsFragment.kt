package de.schlikk.calls

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.preference.CheckBoxPreference
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceGroup
import androidx.preference.SeekBarPreference
import androidx.preference.TwoStatePreference
import org.json.JSONArray



class SettingsFragment(private val permissibleActivity: PermissibleActivity) : PreferenceFragmentCompat() {
    var uniFormatter: UniFormatter? = null
    
    var generalCallVolumePreference: SeekBarPreference? = null
    var toggleNumberFilterPref: CheckBoxPreference? = null
    var editModePref: TuplePreferenceButton? = null
    var addNewNumberFilterPref: EditTextPreference? = null
    var showPrefLogPref: ListPreference? = null
    lateinit var preferenceBasedLogger: PreferenceBasedLogger
    val numberFilters = linkedMapOf<String, NumberFilter>()
    
    var initialiseLastCallsList: ( () -> Unit )? = null

    
    
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        init()
    }
    
    fun init() {
        val context = requireContext()
        uniFormatter = UniFormatter(context)
        val sharedPreferences = context.getSharedPreferences(null, Context.MODE_PRIVATE)
        val volumeManager = VolumeManager(context)

        val preferredGeneralCallVolumeStoreKey = volumeManager.preferredGeneralCallVolumeStoreKey
        val generalCallVolumePreference = findPreference<SeekBarPreference>(
            preferredGeneralCallVolumeStoreKey
        )!!
        this.generalCallVolumePreference = generalCallVolumePreference
        generalCallVolumePreference.min = volumeManager.getAdaptedStreamMinVolume()
        generalCallVolumePreference.max = volumeManager.getStreamMaxVolume()
        generalCallVolumePreference.value = sharedPreferences.getInt(
            generalCallVolumePreference.key,
            generalCallVolumePreference.max
        )
        generalCallVolumePreference.setOnPreferenceChangeListener(::storeInt)
        
        val toggleNumberFilterPref = findPreference<CheckBoxPreference>(context.getString(R.string.toggleNumberFilterStoreKey))!!
        this.toggleNumberFilterPref = toggleNumberFilterPref
        toggleNumberFilterPref.isChecked = sharedPreferences.getBoolean(
            toggleNumberFilterPref.key,
            false
        )
        toggleNumberFilterPref.setOnPreferenceChangeListener(::toggleNumberFilters)
    
        val editModePref = TuplePreferenceButton(findPreference(context.getString(R.string.editModeStoreKey))!!)
        this.editModePref = editModePref
        editModePref.titles = listOf(
            context.getString(R.string.editModeNextEditNumbers),
            context.getString(R.string.editModeNextEditImportance),
            context.getString(R.string.editModeNextEditVolumes)
        )
        editModePref.state = sharedPreferences.getInt(editModePref.key, 0)
        editModePref.preference.isVisible = toggleNumberFilterPref.isChecked
        editModePref.onPreferenceChangeListener = ::changeEditMode
        
        val addNewNumberFilterPref = findPreference<EditTextPreference>(context.getString(R.string.addNewNumberFilterStoreKey))!!
        this.addNewNumberFilterPref = addNewNumberFilterPref
        addNewNumberFilterPref.isVisible = toggleNumberFilterPref.isChecked
        addNewNumberFilterPref.setOnPreferenceChangeListener(::addNewNumberFilter)

        readNumberFilters(addNewNumberFilterPref.parent!!)

        val lastCalls = LastCalls(context)
        val lastCallsListPref = findPreference<ListPreference>(lastCalls.viewLastCallsListStoreKey)!!
        val initialiseLastCallsList = {
            lastCallsListPref.entries = lastCalls.getLastCalls()
            lastCallsListPref.entryValues = lastCallsListPref.entries
        }
        this.initialiseLastCallsList = initialiseLastCallsList
        initialiseLastCallsList()
        lastCallsListPref.setOnPreferenceClickListener {
            initialiseLastCallsList()
            true
        }
        
        val clearLastCallsListPref = findPreference<Preference>(context.getString(R.string.clearLastCallsListStoreKey))!!
        clearLastCallsListPref.setOnPreferenceClickListener {
            lastCalls.clear().also { initialiseLastCallsList() }
        }
    
        preferenceBasedLogger = PreferenceBasedLogger( context, context.getString(R.string.showPrefLogStoreKey) )
        
        val togglePrefLogPref = findPreference<CheckBoxPreference>(context.getString(R.string.togglePrefLogStoreKey))!!
        togglePrefLogPref.isChecked = sharedPreferences.getBoolean(
            togglePrefLogPref.key,
            false
        )
        togglePrefLogPref.setOnPreferenceChangeListener(::togglePrefLog)
    
        val showPrefLogPref = ListPreference(context)
        this.showPrefLogPref = showPrefLogPref
        showPrefLogPref.key = context.getString(R.string.showPrefLogStoreKey)
        showPrefLogPref.title = context.getString(R.string.showPrefLogTitle)
        if( togglePrefLogPref.isChecked ) {
            val prefLogEntries = preferenceBasedLogger.read()
            showPrefLogPref.entries = prefLogEntries
            showPrefLogPref.entryValues = prefLogEntries
        }
        showPrefLogPref.isVisible = togglePrefLogPref.isChecked
        togglePrefLogPref.parent!!.addPreference( showPrefLogPref )
    
        val resetPref = findPreference<Preference>(context.getString(R.string.resetStoreKey))!!
        resetPref.setOnPreferenceClickListener {
            resetSettings(sharedPreferences)
            true
        }
        
        val openPrivacyPolicyPref = findPreference<Preference>(context.getString(R.string.openPrivacyPolicyStoreKey))!!
        openPrivacyPolicyPref.setOnPreferenceClickListener {
            val uri = Uri.parse( getString( R.string.privacyPolicyUrl ) )
            val intent = Intent( Intent.ACTION_VIEW, uri )
            startActivity( intent )
            true
        }
    }
    
    
    
    @SuppressLint("ApplySharedPref")
    fun resetSettings(sharedPreferences: SharedPreferences) {
        sharedPreferences.edit().clear().commit() // commit immediately to prevent false values in user interface
        init()
    }
    
    
    
    fun updateEditModeVisibility(
            mode: Int,
            filters: Collection<NumberFilter> = numberFilters.values
    ) {
        when( mode ) {
            0 -> filters.forEach { it.setValueVisible() }
            1 -> filters.forEach { it.setTextVisible() }
            2 -> filters.forEach { it.setImportanceVisible() }
        }
    }
    
    fun toggleNumberFilters(preference: Preference, numberFilterEnabled: Any) = preference is TwoStatePreference &&
        if( numberFilterEnabled as Boolean ) {
            permissibleActivity.callUnderPermission(
                PermissionCall(
                    "android.permission.READ_PHONE_NUMBERS",
                    requireContext(),
                    R.string.permissionReadPhoneNumbers
                ) {
                    editModePref!!.preference.isVisible = true
                    addNewNumberFilterPref?.isVisible = true
                    updateEditModeVisibility(editModePref!!.state)
                    preference.isChecked = true
                    storeBoolean(preference, true)
                }
            )
    
            false
        }
        else {
            editModePref?.preference?.isVisible = false
            addNewNumberFilterPref?.isVisible = false
            numberFilters.values.forEach { it.setNothingVisible() }
            storeBoolean(preference, false)
            true
        }
    
    fun togglePrefLog(preference: Preference, prefLogEnabled: Any) =
        if( prefLogEnabled as Boolean ) {
            showPrefLogPref?.isVisible = true
            val prefLogEntries = preferenceBasedLogger.read()
            showPrefLogPref?.entries = prefLogEntries
            showPrefLogPref?.entryValues = prefLogEntries
            storeBoolean(preference, true)
            true
        }
        else {
            showPrefLogPref?.isVisible = false
            storeBoolean(preference, false)
            true
        }
    
    fun changeEditMode(preference: TuplePreferenceButton, newValue: Int): Boolean {
        updateEditModeVisibility(newValue)
        storeInt(preference.preference, newValue)
        return true
    }
    
    fun toastNumberIsAlreadyDefined(number: String) {
        Toast.makeText(
            context,
            uniFormatter!!.format(R.string.errorNumberIsAlreadyDefined, number),
            Toast.LENGTH_SHORT
        ).show()
    }

    fun addNewNumberFilter(preference: Preference, numberValue: Any): Boolean {
        val input = ( numberValue as String ).trim()
        if( input.isEmpty() ) return false

        val filter = createNumberFilter(input, generalCallVolumePreference!!.value)
        if( numberFilters.containsKey(filter.getKey()) ) {
            toastNumberIsAlreadyDefined(input)
        }
        else {
            numberFilters[filter.getKey()] = filter
            storeNumberFilter(filter)
            filter.preferences().forEach { preference.parent!!.addPreference(it) }
        }
        return false
    }
    
    fun createNumberFilter(text: String, value: Int): NumberFilter {
        val filter = NumberFilter(
            text,
            value,
            generalCallVolumePreference!!.min,
            generalCallVolumePreference!!.max,
            requireContext()
        )
        
        updateEditModeVisibility(editModePref!!.state, listOf(filter))
        
        filter.keyChangeListener = { numf, oldKey, newKey ->
            if( numberFilters.containsKey(newKey) ) {
                toastNumberIsAlreadyDefined(newKey)
                false
            }
            else {
                numberFilters.remove(oldKey)
                numberFilters[newKey] = numf
                storeNumberFilter(numf)
                true
            }
        }
        filter.importanceChangeListener = { numf, _, newValue ->
            if( newValue ) {
                if( permissibleActivity.requireNotificationPolicyAccess() ) {
                    // store filter with importance
                    storeNumberFilter(numf)
                    true
                }
                else {
                    false
                }
            }
            else {
                // store filter with no importance
                storeNumberFilter(numf)
                true
            }
        }
        filter.valueChangeListener = { numf, _, _ ->
            storeNumberFilter(numf)
        }
        filter.deleteListener = { numf ->
            filter.preferences().forEach { it.parent!!.removePreference(it) }
            numberFilters.remove(numf.getKey())
            deleteNumberFilter(numf)
            true
        }
        return filter
    }


    
    
    
    
    




    fun storeInt(preference: Preference, value: Any) =
        preference.context.getSharedPreferences(null, Context.MODE_PRIVATE)
            .edit()
            .putInt(preference.key, value as Int)
            .apply()
            .let {
                Toast.makeText(
                    context,
                    uniFormatter!!.format(R.string.preferenceSaved, "${preference.title}=$value"),
                    Toast.LENGTH_SHORT
                ).show()
                true
            }
    
    fun storeBoolean(preference: Preference, value: Boolean) =
        preference.context.getSharedPreferences(null, Context.MODE_PRIVATE)
            .edit()
            .putBoolean(preference.key, value)
            .apply()
            .let {
                Toast.makeText(
                    context,
                    uniFormatter!!.format(R.string.preferenceSaved, "${preference.title}=$value"),
                    Toast.LENGTH_SHORT
                ).show()
                true
            }
    
    
    
    
    
    
    fun deleteNumberFilter(filter: NumberFilter): Boolean {
        val jsonArray = JSONArray(numberFilters.keys)
        
        val context = requireContext()
        val editor = context.getSharedPreferences(null, Context.MODE_PRIVATE).edit()
        editor.putString(context.getString(R.string.numbersListStoreKey), jsonArray.toString())
        filter.preferences().forEach { editor.remove(it.key) }
        editor.apply()

        Toast.makeText(
            context,
            uniFormatter!!.format(R.string.preferenceRemoved, filter.getInput()),
            Toast.LENGTH_SHORT
        ).show()
        return true
    }
    
    fun storeNumberFilter(filter: NumberFilter): Boolean {
        val jsonArray = JSONArray(numberFilters.keys)
        
        val context = requireContext()
        val editor = context.getSharedPreferences(null, Context.MODE_PRIVATE).edit()
        editor.putString(context.getString(R.string.numbersListStoreKey), jsonArray.toString())
        filter.store(editor)
        editor.apply()

        Toast.makeText(
            context,
            uniFormatter!!.format(R.string.preferenceSaved, filter.getInput()),
            Toast.LENGTH_SHORT
        ).show()
        return true
    }
    
    fun readNumberFilters(preferenceGroup: PreferenceGroup) {
        val context = requireContext()
        val sharedPreferences = context.getSharedPreferences(null, Context.MODE_PRIVATE)
        val all = sharedPreferences.all

        val numbersListString = all[context.getString(R.string.numbersListStoreKey)]
        if( numbersListString != null ) {
            val numbersList = JSONArray(numbersListString as String)
            for( i in 0 until numbersList.length() ) {
                val key = numbersList.getString(i)
                val filter = createNumberFilter(key, 0)
                filter.read(sharedPreferences)
                numberFilters[filter.getKey()] = filter
                filter.preferences().forEach { preferenceGroup.addPreference(it) }
            }
        }
    }
    
    
    

    override fun onResume() {
        super.onResume()
        initialiseLastCallsList?.invoke()
    }
}