package de.schlikk.calls

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.Context
import android.media.AudioManager
import android.os.Build
import android.os.Build.VERSION
import android.provider.Settings
import android.util.Log

class VolumeManager( val context: Context, val audioStream: Int = AudioManager.STREAM_RING, val preferenceBasedLogger: PreferenceBasedLogger? = null ) {
    val previousVolumeStoreKey = "Previous-Volume-from-AudioStream-$audioStream"
    val previousDoNotDisturbStateStoreKey = "Previous-Do-Not-Disturb-State-from-AudioStream-$audioStream"
    val preferredGeneralCallVolumeStoreKey = context.getString(R.string.preferredGeneralCallVolumeStoreKey)
    val defaultMinimumVolume = 1
    
    private var audioManager: AudioManager? = null
    private var minVolume: Int? = null
    private var maxVolume: Int? = null
    
    
    
    fun requireAudioManager(): AudioManager {
        if( audioManager == null ) {
            audioManager = context.getSystemService( Context.AUDIO_SERVICE ) as AudioManager?
                ?: throw Exception( "Missing AudioManager" )
        }
        return audioManager!!
    }
    
    
    
    class VolumeState( val volume: Int, val isDoNotDisturb: Boolean ) {}
    
    @SuppressLint( "ApplySharedPref" ) // we want to store the data immediately
    fun storeImmediately( state: VolumeState) {
        val sharedPreferences = context.getSharedPreferences( null, Context.MODE_PRIVATE )
        val edit = sharedPreferences.edit()
        edit.putInt( previousVolumeStoreKey, state.volume )
        edit.putBoolean( previousDoNotDisturbStateStoreKey, state.isDoNotDisturb )
        edit.commit()
    }
    
    
    
    fun isDoNotDisturb( context: Context, audioManager: AudioManager ) =
        if( VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 ) {
            val zenMode = Settings.Global.getInt(context.contentResolver, "zen_mode")
            Log.d( "VolumeManager", "ContentResolver[zen_mode] = $zenMode" )
            preferenceBasedLogger?.log( "isDoNotDisturb( ContentResolver[zen_mode] ): 0 != $zenMode" )
            0 != zenMode
        }
        else {
            val streamVolume = audioManager.getStreamVolume(audioStream)
            val streamMinVolume = getStreamMinVolume()
            Log.d( "VolumeManager", "current version ${VERSION.SDK_INT} is lower than JELLY_BEAN_MR1 ${Build.VERSION_CODES.JELLY_BEAN_MR1} so we can only guess DND mode by streamVolume=$streamVolume < streamMinVolume=$streamMinVolume" )
            preferenceBasedLogger?.log( "isDoNotDisturb( ${VERSION.SDK_INT} < JELLY_BEAN_MR1 ): streamVolume $streamVolume < streamMinVolume $streamMinVolume" )
            streamVolume < streamMinVolume
        }
    // 0 == audioManager.getStreamVolume( audioStream ) does only work in emulator but actual phones might allow volume 0
    // see also https://stackoverflow.com/questions/31387137/android-detect-do-not-disturb-status
    
    
    
    fun getAdaptedStreamVolume() =
        if( VERSION.SDK_INT >= Build.VERSION_CODES.M && requireAudioManager().isStreamMute( audioStream ) ) {
            Log.d("VolumeManager", "stream is muted => return volume 0")
            0
        }
        else
            requireAudioManager().getStreamVolume( audioStream )
    fun getStreamMinVolume(): Int {
        if( minVolume == null ) {
            if( VERSION.SDK_INT >= Build.VERSION_CODES.P)
                minVolume = requireAudioManager().getStreamMinVolume( audioStream )
            else
                minVolume = defaultMinimumVolume
        }
        return minVolume!!
    }
    fun getAdaptedStreamMinVolume() =
        if( VERSION.SDK_INT >= Build.VERSION_CODES.M )
            0
        else
            getStreamMinVolume()
    fun getStreamMaxVolume(): Int {
        if( maxVolume == null ) {
            maxVolume = requireAudioManager().getStreamMaxVolume( audioStream )
        }
        return maxVolume!!
    }
    
    
    
    private fun setAdaptedStreamVolume( volumeDecision: VolumeDecision ) {
        if( VERSION.SDK_INT >= Build.VERSION_CODES.M && volumeDecision.mute ) {
            Log.d("VolumeManager", "muting volume")
            preferenceBasedLogger?.log( "muting volume")
            requireAudioManager().adjustStreamVolume( audioStream, AudioManager.ADJUST_SAME, AudioManager.ADJUST_MUTE )
        }
        else {
            Log.d("VolumeManager", "setting volume to $volumeDecision")
            preferenceBasedLogger?.log( "setting volume to $volumeDecision")
            requireAudioManager().setStreamVolume( audioStream, volumeDecision.volume, AudioManager.FLAG_PLAY_SOUND )
        }
    }
    
        
    
    fun setVolumeState( volume: Int?, activateDoNotDisturb: Boolean = false, ignoreDoNotDisturb: Boolean = false ): VolumeState? {
        val audioManager = requireAudioManager()
        
        if( VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && audioManager.isVolumeFixed ) {
            Log.e("VolumeManager", "Volume is fixed -> no adjustment possible")
            return null
        }
        
        val isDoNotDisturb = isDoNotDisturb( context, audioManager )
        if( isDoNotDisturb )
            Log.d("VolumeManager", "Detected >Do Not Disturb< mode")
        if( !ignoreDoNotDisturb && isDoNotDisturb ) {
            Log.d("VolumeManager", " => won't disturb")
            preferenceBasedLogger?.log( " => won't disturb" )
            return null
        }
    
        val oldVolume = getAdaptedStreamVolume()
        Log.d("VolumeManager", "preserving oldVolume $oldVolume")
        preferenceBasedLogger?.log( "current volume is $oldVolume amd doNotDisturb is ${if( isDoNotDisturb ) "active" else "inactive" }" )
        
        val streamMaxVolume = getStreamMaxVolume()
        val newVolume = if( volume != null ) {
                            checkVolumeBounds( volume, getStreamMinVolume(), streamMaxVolume )
                        }
                        else {
                            Log.d("VolumeManager", "no volume specified => using maxVolume")
                            preferenceBasedLogger?.log( "no volume specified => using maxVolume")
                            VolumeDecision( streamMaxVolume )
                        }
        setAdaptedStreamVolume( newVolume )
    
        if(activateDoNotDisturb) {
            if(VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Log.d("VolumeManager", "activating DoNotDisturb")
                preferenceBasedLogger?.log( "activating DoNotDisturb")
                // https://stackoverflow.com/questions/31862753/android-how-to-turn-on-do-not-disturb-dnd-programmatically
                ( context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager )
                    .setInterruptionFilter( NotificationManager.INTERRUPTION_FILTER_NONE )
            }
            else {
                preferenceBasedLogger?.log( "not activating DoNotDisturb because version ${VERSION.SDK_INT} < M ${Build.VERSION_CODES.M}")
            }
        }
        
        return VolumeState( oldVolume, isDoNotDisturb )
    }
    
    
    
    data class VolumeDecision( val volume: Int, val mute: Boolean = false ) {}
    
    fun checkVolumeBounds( volume: Int, streamMinVolume: Int, streamMaxVolume: Int ) =
        when {
            streamMinVolume > streamMaxVolume ->
                throw IllegalArgumentException( "Illegal bounds min=$streamMinVolume <> max=$streamMaxVolume" )
            
            volume > streamMaxVolume -> {
                Log.w(
                    "VolumeManager",
                    "volume=$volume is greater than streamMaxVolume=$streamMaxVolume"
                )
                VolumeDecision( streamMaxVolume )
            }
            
            volume < streamMinVolume -> {
                if( volume == 0 ) {
                    Log.d( "VolumeManager", "volume=$volume is lower than streamMinVolume=$streamMinVolume so guess MUTE is meant" )
                    VolumeDecision( streamMinVolume, mute=true )
                }
                else {
                    Log.w(
                        "VolumeManager",
                        "volume=$volume is lower than streamMinVolume=$streamMinVolume"
                    )
                    VolumeDecision( streamMinVolume )
                }
            }
            
            else -> {
                VolumeDecision( volume )
            }
        }
    
    
    
    fun resetPreviousVolume() {
        val all = context.getSharedPreferences( null, Context.MODE_PRIVATE ).all
        val previousVolume = all[ previousVolumeStoreKey ]
        if( previousVolume != null ) {
            val previousDoNotDisturbState = all[ previousDoNotDisturbStateStoreKey ]
            if( previousDoNotDisturbState != null ) {
                setVolumeState( previousVolume as Int, previousDoNotDisturbState as Boolean, true )
            }
            else {
                setVolumeState( previousVolume as Int )
            }
        }
    }
    
    fun overrideVolume( preferred: NumberFilter? = null ) {
        val previousState =
            if( preferred == null ) {
                val preferredGeneralCallVolume =
                    context.getSharedPreferences( null, Context.MODE_PRIVATE )
                        .all[preferredGeneralCallVolumeStoreKey] as Int?
                setVolumeState( preferredGeneralCallVolume )
            }
            else {
                setVolumeState( preferred.getValue(), false, preferred.getImportance() )
            }
        if( previousState != null )
            storeImmediately( previousState )
    }
}